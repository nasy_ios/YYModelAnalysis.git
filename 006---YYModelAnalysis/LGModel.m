//
//  LGModel.m
//  006---YYModelAnalysis
//
//  Created by cooci on 2018/12/30.
//  Copyright © 2018 cooci. All rights reserved.
//

#import "LGModel.h"

@implementation LGModel

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{@"books" : LGSubModel.class,
             @"infoDict" : [LGPerson class],
             @"likedUserIds" : @"NSNumber"
             };
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"messageId":@[@"id",@"ID",@"Id"],@"createTime":@"timeInterval"};
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic{
    NSNumber *interval = dic[@"timeInterval"];
    if (![interval isKindOfClass:[NSNumber class]]) {
        return NO;
    }
    _createTime = [NSDate dateWithTimeIntervalSince1970:[interval floatValue]];
    return YES;
}

- (BOOL)modelCustomTransformToDictionary:(NSMutableDictionary *)dic {
    if (!_createTime) {
        return NO;
    }
    dic[@"timeInterval"] =@([_createTime timeIntervalSince1970]) ;
    return YES;
}


@end

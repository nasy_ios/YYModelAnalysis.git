//
//  LGSubModel.m
//  006---YYModelAnalysis
//
//  Created by cooci on 2018/12/30.
//  Copyright © 2018 cooci. All rights reserved.
//

#import "LGSubModel.h"

@implementation LGSubModel
/**
 *    黑名单不处理，不会赋值
 */
+(NSArray<NSString *> *)modelPropertyBlacklist {
    return @[@"subject"];
}
/**
 *    白名单，只处理白名单相应的属性
 */
+ (NSArray<NSString *> *)modelPropertyWhitelist {
    return @[@"name",@"age",@"num"];
}

@end

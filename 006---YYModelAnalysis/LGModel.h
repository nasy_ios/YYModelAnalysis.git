//
//  LGModel.h
//  006---YYModelAnalysis
//
//  Created by cooci on 2018/12/30.
//  Copyright © 2018 cooci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel.h>
#import "LGSubModel.h"
#import "LGPerson.h"

NS_ASSUME_NONNULL_BEGIN

@interface LGModel : NSObject<YYModel>
@property (nonatomic, strong) NSArray *books; //Array<对象>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) int messageId;
@property (nonatomic, assign) int age;
/// 时间
@property (nonatomic, strong) NSDate *createTime;

@property (nonatomic, strong) LGSubModel *subModel;
// Key:name(NSString) Value:user(对象)
@property (nonatomic, strong) NSDictionary *infoDict;
@property (nonatomic, strong) NSSet *likedUserIds;// Set<NSNumber>

@end

NS_ASSUME_NONNULL_END

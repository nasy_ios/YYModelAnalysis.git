//
//  LGSubModel.h
//  006---YYModelAnalysis
//
//  Created by cooci on 2018/12/30.
//  Copyright © 2018 cooci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface LGSubModel : NSObject<YYModel>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *subject;
@property (nonatomic, assign) int num;
@property (nonatomic, assign) int age;
@end

NS_ASSUME_NONNULL_END

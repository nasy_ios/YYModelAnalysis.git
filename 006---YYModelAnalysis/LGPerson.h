//
//  LGPerson
//  006---YYModelAnalysis
//
//  Created by cooci on 2018/12/30.
//  Copyright © 2018 cooci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface LGPerson : NSObject<YYModel>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) int gender;
@end

@interface LGMan : LGPerson
@property (nonatomic, copy) NSString *sex;

@end



NS_ASSUME_NONNULL_END

//
//  LGPerson.m
//  006---YYModelAnalysis
//
//  Created by cooci on 2018/12/30.
//  Copyright © 2018 cooci. All rights reserved.
//

#import "LGPerson.h"

@implementation LGPerson
///根据dic来实例不同类的类型
+(Class)modelCustomClassForDictionary:(NSDictionary *)dictionary {
    if ([dictionary[@"gender"] integerValue] == 1) {
        return LGMan.class;
    }
    return self;
}
/**
 *   发生在字典转模型之前，最后对网络字典做一次处理，可以自定义修改dict的内容
 */
- (NSDictionary *)modelCustomWillTransformFromDictionary:(NSDictionary *)dic{
    if ([dic[@"gender"] integerValue] == 1) {
        return nil;//不接受男性
    }
    return dic;
}

@end

@implementation LGMan

@end

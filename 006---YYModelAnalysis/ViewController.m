//
//  ViewController.m
//  006---YYModelAnalysis
//
//  Created by cooci on 2018/12/30.
//  Copyright © 2018 cooci. All rights reserved.
//

#import "ViewController.h"
#import "LGModel.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *dict = @{@"subModel":@{@"name":@"cooci",
                                 @"subject":@"iOS",
                                 @"num":@100,
                                 @"age":@18},
                           @"name":@"Kody",
                           @"age":@17,
                           @"id":@10086,
                           @"timeInterval":@611086848,
                           @"infoDict":@{@"mainEditor":@{@"gender":@0,@"name":@"YYmodel"},@"subEditor":@{@"gender":@1,@"name":@"Nasy"}},
                           @"books":@[
                                   @{@"name":@"HK",
                                     @"subject":@"逆向",
                                     @"num":@100,
                                     @"age":@20},
                                   @{@"name":@"Cooci",
                                     @"subject":@"进阶",
                                     @"num":@99,
                                     @"age":@18},
                                   @{@"name":@"Kody",
                                     @"subject":@"项目",
                                     @"num":@98,
                                     @"age":@17},
                                   @{@"name":@"CC",
                                     @"subject":@"视觉",
                                     @"num":@97,
                                     @"age":@16}],
                           @"likedUserIds":@[@10086,@10000,@100010]
                           };
    
    // cls
    // 发送 setter 信息
    
    LGModel *model = [LGModel yy_modelWithDictionary:dict];
//    [model valueForKeyPath:@"infoDict.subEditor"];
    
    LGModel *model1 = [[LGModel alloc]init];
    [model1 yy_modelSetWithDictionary:dict];
    
    [model yy_modelToJSONObject];
    
}




@end
